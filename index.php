<?php
require_once('vendor/autoload.php');

function real_ip(){
	if ( function_exists( 'apache_request_headers' ) ) {
		$headers = apache_request_headers();
	} else {
		$headers = $_SERVER;
	}
	if ( array_key_exists( 'HTTP_CLIENT_IP', $headers ) && filter_var( $headers['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) { $ip = $headers['HTTP_CLIENT_IP']; }
	elseif ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) { $ip = $headers['X-Forwarded-For']; } 
	elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) { $ip = $headers['HTTP_X_FORWARDED_FOR']; } 
	elseif ( array_key_exists( 'HTTP_X_FORWARDED', $headers ) && filter_var( $headers['HTTP_X_FORWARDED'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) { $ip = $headers['HTTP_X_FORWARDED']; } 
	elseif ( array_key_exists( 'HTTP_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) { $ip = $headers['HTTP_FORWARDED_FOR']; } 
	elseif ( array_key_exists( 'HTTP_FORWARDED', $headers ) && filter_var( $headers['HTTP_FORWARDED'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) { $ip = $headers['HTTP_FORWARDED']; } 
	elseif ( array_key_exists( 'REMOTE_ADDR', $headers ) && filter_var( $headers['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) { $ip = $headers['REMOTE_ADDR']; }
	else { $ip = NULL; }
	return $ip;
}

$curl = new \Ivory\HttpAdapter\CurlHttpAdapter();
$geocoder = new \Geocoder\Provider\GeoPlugin($curl);

$ip = real_ip();
$address = $geocoder->geocode($ip)->first();

$city = $address->getLocality();
$lat = $address->getLatitude();
$lon = $address->getLongitude();

$url = "http://api.openweathermap.org/data/2.5/weather?lat=".$lat."&lon=".$lon."&appid=ffabe529b778277746e08d2984c4faf1&units=metric";
$owm = file_get_contents($url);
$owm_obj = json_decode($owm);

$main = $owm_obj->weather[0]->main;
$description = $owm_obj->weather[0]->description;
$icon = $owm_obj->weather[0]->icon;
$temperature = round($owm_obj->main->temp);

switch ($icon) {
	case '01d':
	case '01n':
		$image = "images/soleado.png";
		break;
	case '02d':
	case '02n':
		$image = "images/medio_nublado.png";
		break;
	case '03d':
	case '03n':
		$image = "images/nublado.png";
		break;
	case '04d':
	case '04n':
		$image = "images/nublado_viento.png";
		break;
	case '09d':
	case '09n':
		$image = "images/lluvia.png";
		break;
	case '10d':
	case '10n':
		$image = "images/lluvia_fuerte.png";
		break;
	case '11d':
	case '11n':
		$image = "images/nublado_lluvia_trueno.png";
		break;
	case '13d':
	case '13n':
		$image = "images/nieve.png";
		break;
}

$size = 17;
$angle = 0;
$fontfile = './fonts/Impact.ttf';
$text = $temperature . '°C';

header("Content-type: image/png");

$input = imagecreatefrompng($image);
list($width, $height) = getimagesize($image);
$output = imagecreatetruecolor($width, $height);
$white = imagecolorallocate($output, 34, 55, 71);
$blue = imagecolorallocate($output, 255, 255, 255);
imagefilledrectangle($output, 0, 0, $width, $height, $white);
imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
$text_box = imagettfbbox($size, $angle, $fontfile, $text);
$text_width = $text_box[2] - $text_box[0];
$x = $width/2 - $text_width/2;
$y = 90;
imagettftext($output, $size, 0, $x, $y, $blue, $fontfile, $text);
imagejpeg($output);
imagedestroy($output);

?>
